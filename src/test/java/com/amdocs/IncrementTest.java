package com.amdocs;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class IncrementTest{

@Test
 public void decreasecounterTestElse() throws Exception {
 int megaloonoma = new Increment().decreasecounter(8);
assertEquals("Else" , 1, megaloonoma);
}

@Test
 public void decreasecounterTestOne() throws Exception {
 int megaloonoma = new Increment().decreasecounter(1);
assertEquals("input one" , 1, megaloonoma);
}

@Test
 public void decreasecounterTestZero() throws Exception {
 int megaloonoma = new Increment().decreasecounter(0);
assertEquals("input zero" , 1, megaloonoma);
}


}


